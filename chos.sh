#!/bin/bash

read -rd '' HELP << ENDHELP
Usage: $(basename $0) DEVICE_NAME

Options:
	-A do a full run
	-d debug mode
ENDHELP

set -e
set -x

DEVICE=$1

# AWS config
AWS_REGION='us-west-2'
AWS_KEYS_BUCKET='unofficial-copperheados-keys'
AWS_RELEASE_BUCKET='unofficial-copperheados-release'
AWS_LOGS_BUCKET='unofficial-copperheados-logs'
AWS_SNS_ARN='arn:aws:sns:us-west-2:088146550601:chos'

# targets
BUILD_TARGET="release aosp_${DEVICE} user"
RELEASE_CHANNEL="${DEVICE}-stable"

CHOS_DIR="$HOME/copperheados"
DEPOT_TOOLS_PATH="${CHOS_DIR}/depot_tools"
CERTIFICATE_SUBJECT='/CN=Unofficial CopperheadOS'
OFFICIAL_RELEASE_URL='https://release.copperhead.co'
UNOFFICIAL_RELEASE_URL="https://${AWS_RELEASE_BUCKET}.s3.amazonaws.com"

export PATH="${PATH}:${DEPOT_TOOLS_PATH}"

read -ra metadata <<< "$(wget --quiet -O - "${OFFICIAL_RELEASE_URL}/${RELEASE_CHANNEL}")"
OFFICIAL_DATE="${metadata[0]}"
OFFICIAL_TIMESTAMP="${metadata[1]}"
OFFICIAL_VERSION="${metadata[2]}"
TAG="${OFFICIAL_VERSION}.${OFFICIAL_DATE}"
BRANCH="refs/tags/${TAG}"

VENDOR_VERSION=

CHROMIUM_REVISION="$(wget --quiet -O - "https://raw.githubusercontent.com/CopperheadOS/chromium_patches/${TAG}/args.gn" | awk /android_default_version_name/'{print $3}' - |  cut --delimiter '"' --fields 2)"



# make getopts ignore $1 since it is $DEVICE
OPTIND=2
FULL_RUN='false'
DEBUG_FLAG='false'
while getopts ":hAd" opt; do
  case $opt in
    h)
      echo "${HELP}"
      ;;
    A)
      FULL_RUN='true'
      ;;
    d)
      DEBUG_FLAG='true'
      ;;
    \?)
      echo "${HELP}"
      ;;
  esac
done



full_run_chos() {
  setup_env
  fetch_chos
  setup_vendor
  aws_import_keys
  patch_chos
  full_run_chromium
  build_chos
  aws_release
}

full_run_chromium() {
  fetch_chromium
  setup_chromium_deps
  patch_chromium
  build_chromium
}

setup_env() {
  ubuntu_setup_packages
  setup_git
  setup_gpg
  aws_setup_chos_dir
  setup_depot_tools
}

fetch_chos() {
  pushd "${CHOS_DIR}"
  repo init --manifest-url 'https://github.com/CopperheadOS/platform_manifest.git' --manifest-branch "${BRANCH}"
  verify_manifest
  sed --in-place --expression '\@<project path="external/chromium" name="platform_external_chromium" remote="bitbucket" />@d' "${CHOS_DIR}/.repo/manifest.xml" || true
  for i in {1..10}; do
    repo sync --jobs 32 && break
  done
  verify_source
}

patch_chos() {
  patch_manifest
  patch_updater
  patch_priv_ext
}

build_chos() {
  pushd "$CHOS_DIR"
  source "${CHOS_DIR}/script/copperhead.sh"

  choosecombo $BUILD_TARGET
  make -j $(nproc) target-files-package
  make -j $(nproc) brillo_update_payload

  "${CHOS_DIR}/script/release.sh" "$DEVICE"
}



ubuntu_setup_packages() {
  sudo apt-get update
  sudo apt-get --assume-yes install openjdk-8-jdk git-core gnupg flex bison build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z-dev ccache libgl1-mesa-dev libxml2-utils xsltproc unzip python-networkx liblz4-tool
  sudo apt-get --assume-yes build-dep "linux-image-$(uname --kernel-release)"
}

setup_git() {
  git config --get --global user.name || git config --global user.name 'user'
  git config --get --global user.email || git config --global user.email 'user@localhost'
  git config --global color.ui true 
}

setup_gpg() {
  # init gpg
  gpg --list-keys
  # receiving keys sometimes fails
  gpg --recv-keys '65EEFE022108E2B708CBFCF7F9E712E59AF5F22A' || gpg --recv-keys '9AF5F22A'
  gpg --recv-keys '4340D13570EF945E83810964E8AD3F819AB10E78' || gpg --recv-keys '9AB10E78'
}

aws_setup_chos_dir() {
  mkdir --parents "$CHOS_DIR"
  case "$(curl 'http://169.254.169.254/latest/meta-data/instance-type')" in
    c3.2xlarge|c3.4xlarge|c3.8xlarge)
      sudo umount /mnt
      sudo mkfs.btrfs -d raid0 /dev/xvdca /dev/xvdcb -f
      sudo mount /dev/xvdca "$CHOS_DIR" -o compress=lzo
      ;;
    c5d.2xlarge|c5d.4xlarge|c5d.9xlarge)
      sudo mkfs.btrfs /dev/nvme1n1 -f
      sudo mount /dev/nvme1n1 "$CHOS_DIR" -o compress=lzo
      ;;
    c5d.18xlarge)
      sudo mkfs.btrfs -d raid0 /dev/nvme1n1 /dev/nvme2n1 -f
      sudo mount /dev/nvme1n1 "$CHOS_DIR" -o compress=lzo
      ;;
  esac
  sudo chown -R ubuntu:ubuntu "$CHOS_DIR"
}

setup_depot_tools() {
  cd "${CHOS_DIR}"
  git clone 'https://chromium.googlesource.com/chromium/tools/depot_tools.git'

}

verify_manifest() {
  pushd "${CHOS_DIR}/.repo/manifests"
  git verify-tag --raw "$(git describe)"
}

verify_source() {
  pushd "${CHOS_DIR}/.repo/manifests"
  repo forall --command 'git verify-tag --raw $REPO_RREV'
}

# call with argument: .x509.pem file
fdpe_hash() {
  keytool -list -printcert -file "$1" | grep 'SHA256:' | tr --delete ':' | cut --delimiter ' ' --fields 3
}

patch_manifest() {
  pushd "$CHOS_DIR"/device/google/marlin
  perl -0777 -i.original -pe 's/ifeq \(\$\(OFFICIAL_BUILD\),true\)\n    PRODUCT_PACKAGES \+= Updater\nendif/PRODUCT_PACKAGES \+= Updater/' device-common.mk
}

patch_updater() {
  pushd "$CHOS_DIR"/packages/apps/Updater/res/values
  sed --in-place \
    --expression "s@${OFFICIAL_RELEASE_URL}@${UNOFFICIAL_RELEASE_URL}@g" config.xml
}

patch_priv_ext() {
  official_sailfish_releasekey_hash='B919FFF979EAC18DF3E65C6D2EBE63F393F11B4BAB344ADE255B2465F49836BC'
  official_sailfish_platform_hash='1C3FBC736E9B7B09E309B8379FF954BF5BD9F95ED399741D7D1D3A42F8ADB757'
  official_marlin_releasekey_hash='6425C9DE6219056CCE62F73E7AD9F92C940B83BAC1D5516ABEBCE1D38F85E4CF'
  official_marlin_platform_hash='CC1E06EAD3E9CA2C4E46073172E92BAD4AFB02D4D21EDDC3F4D9A50C2FBD639D'

  unofficial_sailfish_releasekey_hash=$(fdpe_hash "${CHOS_DIR}/keys/sailfish/releasekey.x509.pem")
  unofficial_sailfish_platform_hash=$(fdpe_hash "${CHOS_DIR}/keys/sailfish/platform.x509.pem")
  unofficial_marlin_releasekey_hash=$(fdpe_hash "${CHOS_DIR}/keys/marlin/releasekey.x509.pem")
  unofficial_marlin_platform_hash=$(fdpe_hash "${CHOS_DIR}/keys/marlin/platform.x509.pem")

  sed --in-place \
    --expression "s/${official_marlin_releasekey_hash}/${unofficial_marlin_releasekey_hash}/g" \
    --expression "s/${official_marlin_platform_hash}/${unofficial_marlin_platform_hash}/g" \
    --expression "s/${official_sailfish_releasekey_hash}/${unofficial_sailfish_releasekey_hash}/g" \
    --expression "s/${official_sailfish_platform_hash}/${unofficial_sailfish_platform_hash}/g" \
    "${CHOS_DIR}/packages/apps/F-Droid/privileged-extension/app/src/main/java/org/fdroid/fdroid/privileged/ClientWhitelist.java"
}

aws_import_keys() {
  if [ "$(aws s3 ls "s3://${AWS_KEYS_BUCKET}/${DEVICE}" | wc -l)" == '0' ]; then
    aws_gen_keys
  else
    mkdir "${CHOS_DIR}/keys"
    aws s3 sync "s3://${AWS_KEYS_BUCKET}" "${CHOS_DIR}/keys"
    ln --verbose --symbolic "${CHOS_DIR}/keys/${DEVICE}/verity_user.der.x509" "${CHOS_DIR}/kernel/google/marlin/verity_user.der.x509"
  fi
}

setup_vendor() {
  pushd "${CHOS_DIR}/vendor/android-prepare-vendor"

  if [ "$(git log)" != "*6d9a646afc742e0eed834644b3e2eaefedd82f9e*" ]; then
    sudo apt-get --assume-yes install fuseext2 android-tools-fsutils

    #sed --in-place \
    #  --expression "s/USE_DEBUGFS=true/USE_DEBUGFS=false/g" \
    #  --expression "s/# SYS_TOOLS+=("fusermount")/SYS_TOOLS+=("fusermount")/g" \
    #  --expression "s/# _UMOUNT="fusermount -u"/_UMOUNT="fusermount -u"/g" \
    #  "${CHOS_DIR}/vendor/android-prepare-vendor/execute-all.sh"

    patch -p1 <<'ENDDEBUGFSPATCH'
diff --git a/execute-all.sh b/execute-all.sh
index 41c9713..74f3995 100755
--- a/execute-all.sh
+++ b/execute-all.sh
@@ -321,9 +321,9 @@ if isDarwin; then
   _UMOUNT=umount
 else
   # For Linux use debugfs
-  USE_DEBUGFS=true
-  # SYS_TOOLS+=("fusermount")
-  # _UMOUNT="fusermount -u"
+  USE_DEBUGFS=false
+  SYS_TOOLS+=("fusermount")
+  _UMOUNT="fusermount -u"
 fi
 
 # Check that system tools exist
ENDDEBUGFSPATCH
  fi

  # workaround intermittent download failures, see:
  # https://github.com/anestisb/android-prepare-vendor/issues/126
  vendor_execute-all || { patch_execute-all && vendor_execute-all; }

  aws s3 cp - "s3://${AWS_RELEASE_BUCKET}/${DEVICE}-vendor" --acl public-read <<< "${VENDOR_VERSION}" || true

  mkdir --parents "${CHOS_DIR}/vendor/google_devices" || true
  rm --recursive --force "${CHOS_DIR}/vendor/google_devices/$DEVICE" || true
  mv "${CHOS_DIR}/vendor/android-prepare-vendor/${DEVICE}/$(tr '[:upper:]' '[:lower:]' <<< "${VENDOR_VERSION}")/vendor/google_devices/${DEVICE}" "${CHOS_DIR}/vendor/google_devices"

  if [ "$DEVICE" == 'sailfish' ]; then
    rm --recursive --force "${CHOS_DIR}/vendor/google_devices/marlin" || true
    mv "${CHOS_DIR}/vendor/android-prepare-vendor/sailfish/$(tr '[:upper:]' '[:lower:]' <<< "${VENDOR_VERSION}")/vendor/google_devices/marlin" "${CHOS_DIR}/vendor/google_devices"
  fi

  popd
}

vendor_execute-all() {
  pushd "${CHOS_DIR}/vendor/android-prepare-vendor"
  {
    # try newest version
    ( "${CHOS_DIR}/vendor/android-prepare-vendor/execute-all.sh" --device "${DEVICE}" --buildID "${OFFICIAL_VERSION}" --output "${CHOS_DIR}/vendor/android-prepare-vendor" ) && VENDOR_VERSION="$OFFICIAL_VERSION"
  } || {
    # fallback to known working version
    read -ra VENDOR_VERSION <<< "$(wget -O - "${UNOFFICIAL_RELEASE_URL}/${DEVICE}-vendor")"
    ( "${CHOS_DIR}/vendor/android-prepare-vendor/execute-all.sh" --device "${DEVICE}" --buildID "${VENDOR_VERSION}" --output "${CHOS_DIR}/vendor/android-prepare-vendor" )
  }
  popd
}

patch_execute-all() {
  sed -i "s@url=.*@url=\$\(curl -L --silent \\'https:\/\/developers.google.com\/android\/images\\'\ | \\\@g" "${CHOS_DIR}/vendor/android-prepare-vendor/scripts/download-nexus-image.sh"
}

fetch_chromium() {
  mkdir "${CHOS_DIR}/chromium" || true
  pushd "${CHOS_DIR}/chromium"

  git clone 'https://github.com/CopperheadOS/chromium_patches.git' --branch "${TAG}"

  fetch --nohooks android --target_os_only=true

  gclient sync --with_branch_heads --revision="${CHROMIUM_REVISION}" --jobs 32 --nohooks
}

setup_chromium_deps() {
  pushd "${CHOS_DIR}/chromium"
  yes | gclient runhooks

  echo 'ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true' | sudo debconf-set-selections
  "${CHOS_DIR}/chromium/src/build/install-build-deps-android.sh"

  "${CHOS_DIR}/chromium/src/build/linux/sysroot_scripts/install-sysroot.py" --arch=i386 || true
  "${CHOS_DIR}/chromium/src/build/linux/sysroot_scripts/install-sysroot.py" --arch=amd64 || true
}

patch_chromium() {
  pushd "${CHOS_DIR}/chromium/src"
  git am ${CHOS_DIR}/chromium/chromium_patches/*.patch
}

build_chromium() {
  mkdir --parents "${CHOS_DIR}/chromium/src/out/Default" || true
  ln -s "${CHOS_DIR}/chromium/chromium_patches/args.gn" "${CHOS_DIR}/chromium/src/out/Default/args.gn"

  pushd "${CHOS_DIR}/chromium/src"
  gn gen "${CHOS_DIR}/chromium/src/out/Default"

  pushd "${CHOS_DIR}/chromium/src/out/Default"
  ninja monochrome_public_apk

  mkdir --parents "${CHOS_DIR}/external/chromium/prebuilt/arm64" || true
  cp --verbose "${CHOS_DIR}/chromium/src/out/Default/apks/MonochromePublic.apk" "${CHOS_DIR}/external/chromium/prebuilt/arm64/MonochromePublic.apk"

  aws s3 cp "${CHOS_DIR}/chromium/src/out/Default/apks/MonochromePublic.apk" "s3://${AWS_RELEASE_BUCKET}/chromium/MonochromePublic.apk" --acl public-read &&
  echo "${CHROMIUM_REVISION}" | aws s3 cp - "s3://${AWS_RELEASE_BUCKET}/chromium/revision" --acl public-read

  rm -rf "${CHOS_DIR}/chromium/src/third_party/"
}

aws_release() {
  pushd "${CHOS_DIR}/out"
  build_date="$(< build_number.txt)"
  build_timestamp="$(unzip -p "release-${DEVICE}-${build_date}/${DEVICE}-ota_update-${build_date}.zip" META-INF/com/android/metadata | grep 'post-timestamp' | cut --delimiter "=" --fields 2)"

  read -r old_metadata <<< "$(wget -O - "${UNOFFICIAL_RELEASE_URL}/${RELEASE_CHANNEL}")"
  old_date="$(cut -d ' ' -f 1 <<< "${old_metadata}")"
  (
  aws s3 cp "${CHOS_DIR}/out/release-${DEVICE}-${build_date}/${DEVICE}-ota_update-${build_date}.zip" "s3://${AWS_RELEASE_BUCKET}" --acl public-read &&
  echo "${build_date} ${build_timestamp} ${OFFICIAL_VERSION}" | aws s3 cp - "s3://${AWS_RELEASE_BUCKET}/${RELEASE_CHANNEL}" --acl public-read &&
  echo "${OFFICIAL_TIMESTAMP}" | aws s3 cp - "s3://${AWS_RELEASE_BUCKET}/${RELEASE_CHANNEL}-true-timestamp" --acl public-read
  ) && ( aws s3 rm "s3://${AWS_RELEASE_BUCKET}/${DEVICE}-ota_update-${old_date}.zip" || true )

  #aws s3 cp "${CHOS_DIR}/out/release-${DEVICE}-${build_date}/${DEVICE}-factory-${build_date}.tar.xz" "s3://${AWS_RELEASE_BUCKET}/${DEVICE}-factory-latest.tar.xz" --acl public-read

  if [ "$(aws s3 ls "s3://${AWS_RELEASE_BUCKET}/${DEVICE}-target" | wc -l)" != '0' ]; then
    aws_gen_deltas
  fi
  aws s3 cp "${CHOS_DIR}/out/release-${DEVICE}-${build_date}/${DEVICE}-target_files-${build_date}.zip" "s3://${AWS_RELEASE_BUCKET}/${DEVICE}-target/${DEVICE}-target-files-${build_date}.zip" --acl public-read
}

check_release() {
  pushd /tmp

  read -r unofficial_true_timestamp <<< "$(wget -O - "${UNOFFICIAL_RELEASE_URL}/${RELEASE_CHANNEL}-true-timestamp")"
  ((unofficial_true_timestamp == OFFICIAL_TIMESTAMP)) || return 1

  wget "${UNOFFICIAL_RELEASE_URL}/${DEVICE}-ota_update-${build_date}.zip" || return 2
  build_timestamp="$(unzip -p "${DEVICE}-ota_update-${build_date}.zip" META-INF/com/android/metadata | grep 'post-timestamp' | cut --delimiter "=" --fields 2)"
  ((build_timestamp == OFFICIAL_TIMESTAMP)) || return 3

  return 0
}

aws_gen_deltas() {
  aws s3 sync "s3://${AWS_RELEASE_BUCKET}/${DEVICE}-target" "${CHOS_DIR}/${DEVICE}-target"
  pushd "${CHOS_DIR}/out"
  current_date="$(< build_number.txt)"
  pushd "${CHOS_DIR}/${DEVICE}-target"
  for target_file in ${DEVICE}-target-files-*.zip ; do
    old_date=$(echo "$target_file" | cut --delimiter "-" --fields 4 | cut --delimiter "." --fields 5 --complement)
    pushd "${CHOS_DIR}"
    "${CHOS_DIR}/build/tools/releasetools/ota_from_target_files" --block --package_key "${CHOS_DIR}/keys/${DEVICE}/releasekey" \
    --incremental_from "${CHOS_DIR}/${DEVICE}-target/${DEVICE}-target-files-${old_date}.zip" \
    "${CHOS_DIR}/out/release-${DEVICE}-${current_date}/${DEVICE}-target_files-${current_date}.zip" \
    "${CHOS_DIR}/out/release-${DEVICE}-${current_date}/${DEVICE}-incremental-${old_date}-${current_date}.zip"
    popd
  done
  for incremental in ${CHOS_DIR}/out/release-${DEVICE}-${current_date}/${DEVICE}-incremental-*-*.zip ; do
    aws s3 cp "$incremental" "s3://${AWS_RELEASE_BUCKET}/" --acl public-read && aws s3 rm "s3://${AWS_RELEASE_BUCKET}/${DEVICE}-target/${DEVICE}-target-files-${old_date}.zip"
  done
}

aws_notify() {
  aws sns publish --region "${AWS_REGION}" --topic-arn "$AWS_SNS_ARN" --message "$(date) | CopperheadOS | ${RELEASE_CHANNEL} | ${TAG} | $1"
}

aws_logging() {
  df -h
  du -chs "${CHOS_DIR}"
  uptime
  aws s3 cp '/var/log/cloud-init-output.log' "s3://${AWS_LOGS_BUCKET}/${DEVICE}/$(date +%s)"
}

aws_gen_keys() {
  gen_keys
  aws s3 sync "${CHOS_DIR}/keys" "s3://${AWS_KEYS_BUCKET}"
}

gen_keys() {
  mkdir --parents "${CHOS_DIR}/keys/${DEVICE}"
  pushd "${CHOS_DIR}/keys/${DEVICE}"
  for key in {releasekey,platform,shared,media,verity} ; do
    # make_key exits with unsuccessful code 1 instead of 0
    "${CHOS_DIR}/development/tools/make_key" "$key" "$CERTIFICATE_SUBJECT" || true
  done

  gen_verity_key "${DEVICE}"
}

gen_verity_key() {
  pushd "$CHOS_DIR"

  make -j 20 generate_verity_key
  "${CHOS_DIR}/out/host/linux-x86/bin/generate_verity_key" -convert "${CHOS_DIR}/keys/$1/verity.x509.pem" "${CHOS_DIR}/keys/$1/verity_key"
  make clobber

  openssl x509 -outform der -in "${CHOS_DIR}/keys/$1/verity.x509.pem" -out "${CHOS_DIR}/keys/$1/verity_user.der.x509"
  ln --verbose --symbolic "${CHOS_DIR}/keys/$1/verity_user.der.x509" "${CHOS_DIR}/kernel/google/marlin/verity_user.der.x509"
}

cleanup() {
  if [[ "${DEBUG_FLAG}" == 'true' ]]; then
    sudo shutdown -h now
  fi
}

aws_cleanup() {
  check_release && aws_notify 'SUCCESS' || aws_notify 'FAIL'
  aws_logging
  cleanup
}

trap aws_cleanup 0

aws_notify 'START'

if [[ "${FULL_RUN}" == 'true' ]]; then
  full_run_chos
fi
