#!/usr/bin/env python3
import boto3
import base64
from urllib.request import urlopen
from urllib.request import HTTPError
from datetime import datetime, timedelta

OFFICIAL_URL = 'https://release.copperhead.co/'
UNOFFICIAL_URL = 'https://unofficial-copperheados-release.s3.amazonaws.com/'

SRC_PATH = 's3://unofficial-copperheados/chos.sh'

FLEET_ROLE = 'arn:aws:iam::088146550601:role/aws-ec2-spot-fleet-role'
IAM_PROFILE = 'arn:aws:iam::088146550601:instance-profile/chos-ec2'
AMI_ID = 'ami-0def3275'
SPOT_PRICE = '0.20'

def lambda_handler(event, context):
    for device in ["marlin", "sailfish"]:
        print("checking {0}".format(device))

        official_timestamp = int(urlopen(OFFICIAL_URL + device + '-stable').read().split()[1])
        print("timestamp {0} at {1}".format(official_timestamp, OFFICIAL_URL + device + '-stable'))

        try:
            unofficial_timestamp = int(urlopen(UNOFFICIAL_URL + device + '-stable-true-timestamp').read())
        except HTTPError:
            print("unofficial timestamp not found, defaulting to making a build")
            unofficial_timestamp = 0
        print("timestamp {0} at {1}".format(unofficial_timestamp, UNOFFICIAL_URL + device + '-stable-true-timestamp'))

        if unofficial_timestamp < official_timestamp:
            print("spinning up {0} release".format(device))

            userdata = base64.b64encode("""
#cloud-config
output : {{ all : '| tee -a /var/log/cloud-init-output.log' }}

repo_update: true
repo_upgrade: all
packages:
 - awscli

runcmd:
 - [ bash, -c, "sudo -u ubuntu aws s3 cp {0} /home/ubuntu/chos.sh" ]
 - [ bash, -c, "sudo -u ubuntu bash /home/ubuntu/chos.sh {1} -A" ]
            """.format(SRC_PATH, device).encode('ascii')).decode('ascii')

            client = boto3.client('ec2')
            response = client.request_spot_fleet(
                SpotFleetRequestConfig={
                    'AllocationStrategy': 'lowestPrice',
                    'IamFleetRole': FLEET_ROLE,
                    'LaunchSpecifications': [
                        {
                            'ImageId': AMI_ID,
                            'InstanceType': 'c5d.2xlarge',
                            'UserData': userdata,
                            'BlockDeviceMappings': [
                                {
                                    'DeviceName' : '/dev/sda1',
                                    'Ebs': {
                                        'DeleteOnTermination': True,
                                        'VolumeSize': 16,
                                        'VolumeType': 'gp2',
                                    },
                                },
                            ],
                            'IamInstanceProfile': {
                                'Arn': IAM_PROFILE
                            },
                        },
                    ],
                    'SpotPrice': SPOT_PRICE,
                    'TargetCapacity': 1,
                    'Type': 'request'
                },
            )
            print(response)
