# Introduction
Due to the unavailability of official CopperheadOS builds on Google Pixel and Pixel XL phones not purchased from the CopperheadOS store, a Pixel owner switching to CopperheadOS would have to make an unofficial build by following the [documentation](https://copperhead.co/android/docs/building), laboriously repeating the process when updates are released and [sideloading](https://copperhead.co/android/docs/install#sideloading) updates developer-style. 

[unofficial-copperheados](https://gitlab.com/hminz/unofficial-copperheados) makes life easier for those users by providing an unofficial backend, built on Amazon Web Services, for automated building and over-the-air updates. 

# Architecture

### EC2 Script
The entire build process is conveniently wrapped up into a robust Bash script which, in addition to use by EC2 instances, can also be easily used standalone by those wanting to make a quick one-off build on a local machine.

### Lambda Function
The Lambda function checks for updates by comparing the official and unofficial release metadata and kicks off EC2 instances for building.

### Overview
![architecture](architecture.png)

1. Scheduled CloudWatch event rule triggers Lambda function

2. Lambda function pulls metadata from official release server

3. Lambda function pulls metadata from unofficial release S3 bucket

4. Lambda function launches EC2 spot instance to start building a new release

5. EC2 pulls required files such as signing keys from S3

6. EC2 alerts via SNS to maintainer on error

7. EC2 uploads built release files and metadata to S3

8. Unofficial CopperheadOS client pulls release files from S3
